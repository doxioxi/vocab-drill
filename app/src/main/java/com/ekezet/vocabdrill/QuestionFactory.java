package com.ekezet.vocabdrill;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.ekezet.vocabdrill.kvtml.Kvtml;

public final class QuestionFactory
{
	private HashMap<String, Kvtml.Entry> mOrigEntries;

	@SuppressWarnings("unchecked")
	public QuestionFactory(HashMap<String, Kvtml.Entry> origEntries)
	{
		mOrigEntries = (HashMap<String, Kvtml.Entry>) origEntries.clone();
	}

	/**
	 * Returns a question with a specified number of potential answers.
	 *
	 * @param entries
	 * @param maxItems
	 * @return
	 */
	public Question create(HashMap<String, Kvtml.Entry> entries, int maxItems)
	{
		if (entries.size() == 0 || maxItems <= 0)
			return null;
		List<Kvtml.Entry> localEntries = new ArrayList<Kvtml.Entry>();
		localEntries.addAll(entries.values());
		// pick a solution
		Object[] entryKeys = entries.keySet().toArray();
		final Random rnd = new Random(System.nanoTime() * Double.doubleToLongBits(Math.random()));
		String solutionEntryId = String.valueOf(entryKeys[rnd.nextInt(entryKeys.length)]);
		Kvtml.Entry solution = entries.get(solutionEntryId);
		// shuffle list of entries
		List<Kvtml.Entry> tmp = new ArrayList<Kvtml.Entry>();
		tmp.add(solution);
		Collections.shuffle(localEntries, rnd);
		int ct = 0;
		for (Kvtml.Entry entry : localEntries)
		{
			if (ct == (maxItems - 1))
				break;
			if (entry.id.equals(solution.id))
				continue;
			tmp.add(entry);
			ct++;
		}
		Collections.shuffle(tmp, rnd);
		// create questions
		HashMap<String, Kvtml.Entry> questions = new HashMap<String, Kvtml.Entry>();
		int N = tmp.size() < maxItems ? tmp.size() : maxItems;
		Kvtml.Entry entry = null;
		for (int n = 0; n < N; n++)
		{
			entry = tmp.get(n);
			questions.put(entry.id, entry);
		}
		if (Config.forceChoiceNumber && (questions.size() < maxItems))
		{
			List<Kvtml.Entry> origEntries = new ArrayList<Kvtml.Entry>();
			origEntries.addAll(mOrigEntries.values());
			Collections.shuffle(origEntries, rnd);
			ct = 0;
			int diff = maxItems - questions.size();
			for (int n = 0, max = origEntries.size(); n < max; n++)
			{
				if (ct == diff)
					break;
				entry = origEntries.get(n);
				if (questions.containsKey(entry.id))
					continue;
				questions.put(entry.id, entry);
				ct++;
			}
		}
		return new Question(questions, solution, rnd);
	}
}