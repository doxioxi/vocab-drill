package com.ekezet.vocabdrill.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.dialogs.RecentFilesDialog.RecentItem;

/**
 * A simple Adapter for dealing with an array of filenames.
 *
 * @author Kiripolszky Károly <karcsi@ekezet.com>
 * @see <a href="http://www.youtube.com/watch?v=wDBM6wVEO70">Google I/O 2010 -
 *      The world of ListView</a>
 */
public class RecentFilesAdapter extends ArrayAdapter<RecentItem>
{
	/**
	 * Static helper class for caching views.
	 *
	 * @author Kiripolszky Károly <karcsi@ekezet.com>
	 */
	private static class ViewHolder
	{
		public TextView text;
		public TextView text2;
	}

	private int mViewResource;

	/**
	 * Frequently used static handle for the layout inflater service.
	 */
	private static LayoutInflater sInflater;

	/**
	 * Overridden ArrayAdapter constructor.
	 */
	public RecentFilesAdapter(Context context, int textViewResourceId)
	{
		super(context, textViewResourceId);
		// get and store a handle for the layout inflater
		sInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mViewResource = textViewResourceId;
		Log.i("Debug","mViewResource set by constructor: " + mViewResource);
	}

	/**
	 * Returns the proper view to display a filename.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// prepare view cache
		ViewHolder holder;
		// view cache verification...
		if (convertView == null)
		{
			Log.i("Debug","mViewResource in getView: " + mViewResource);
			// ...inflate new layout resource
			convertView = sInflater.inflate(mViewResource, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.rowtext3);
			if (holder.text==null)
				Log.i("Debug","rowtext zero view given");
			holder.text.setTextColor(Color.WHITE);
			holder.text2 = (TextView) convertView.findViewById(R.id.rowtext4);
			if (holder.text2==null)
				Log.i("Debug","rowtext2 zero view given");
			// cache layout data as view tag
			convertView.setTag(holder);
		} else
			// ...use cached resource
			holder = (ViewHolder) convertView.getTag();

		// format list item (FIXME!)
		RecentItem entry = getItem(position);

		holder.text.setText(entry.getName());
		Log.i("Debug","text: " + holder.text.toString());
		Log.i("Debug","text2: " + holder.text2.toString());
		holder.text2.setText(entry.getDisplayPath());
		//holder.text2.setText("");
		// return item view
		return convertView;
	}
}
