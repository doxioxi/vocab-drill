package com.ekezet.vocabdrill.helpers;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;

public class AssetsUtils
{
	public static String readText(Context context, String filename) throws IOException
	{
		String ret = null;
		AssetManager am = context.getAssets();
		InputStream is = am.open(filename);
		final int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();
		ret = new String(buffer);
		return ret;
	}

	public static String[] listFiles(Context context, String path)
	{
		AssetManager am = context.getAssets();
		try
		{
			return am.list(path);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}