package com.ekezet.vocabdrill.helpers;

import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.models.EntryModel;
import com.ekezet.vocabdrill.models.FileModel;

public final class StatsOpenHelper extends SQLiteOpenHelper
{
	/**
	 * Name of the table that holds the mistaken entries.
	 */
	public static final String TABLE_ENTRIES = "entries";

	/**
	 * Name of the table that holds the names of the known files that have stats.
	 */
	public static final String TABLE_FILES = "files";

	/**
	 * Initial version is 10. Please increment upon changes in structure.
	 */
	private static final int DATABASE_VERSION = 10;

	private static final String DATABASE_NAME = "stats.db";

	/**
	 * Holds the singleton instance of this class.
	 */
	private static StatsOpenHelper _instance = null;

	private int mTestType = -1;

	private StatsOpenHelper(Context context, int testType)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		mTestType = testType;
	}

	public static StatsOpenHelper getInstance(Context context, int testType)
	{
		if (_instance == null)
			_instance = new StatsOpenHelper(context, testType);
		return _instance;
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(FileModel.CREATE_STATEMENT);
		db.execSQL(EntryModel.CREATE_STATEMENT);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRIES);
		onCreate(db);
	}

	/**
	 * Returns an identifier for a file in the "files" table.
	 *
	 * @param name A filename.
	 * @return
	 */
	public long getFileId(String name)
	{
		long rowid = -1;
		SQLiteDatabase rdb = null;
		try
		{
			rdb = getReadableDatabase();
		}
		catch (Exception e)
		{
			if (rdb != null)
				rdb.close();
			return rowid;
		}

		Cursor result = rdb.query(false, TABLE_FILES, FileModel.fields,
			String.format("name='%s'", name), null, null, null, null, "1");

		if (result == null)
		{
			rdb.close();
			return rowid;
		}

		if (result.getCount() == 0)
		{
			ContentValues values = new ContentValues();
			values.put("name", name);
			SQLiteDatabase wdb = getWritableDatabase();
			rowid = wdb.insert(TABLE_FILES, null, values);
			wdb.close();
			rdb.close();
			return rowid;
		}

		result.moveToFirst();
		rowid = result.getLong(0);
		result.close();
		rdb.close();
		return rowid;
	}

	public long insertEntry(EntryModel entry)
	{
		SQLiteDatabase wdb = null;
		long id = -1;
		try
		{
			wdb = getWritableDatabase();
			id = wdb.insert(TABLE_ENTRIES, null, entry.getValues());
		}
		catch (Exception e)
		{
			if (wdb != null)
				wdb.close();
			return -1;
		}
		wdb.close();
		return id;
	}

	/**
	 * Gets the id of an entry in the specific file with a specific translation.
	 *
	 * @param fileId
	 * @param entryId
	 * @param translationId
	 * @return
	 */
	public long getEntryId(long fileId, String entryId, String translationId)
	{
		long rowid = -1;
		SQLiteDatabase rdb = null;

		try
		{
			rdb = getReadableDatabase();
		}
		catch (Exception e)
		{
			if (rdb != null)
				rdb.close();
			return rowid;
		}

		Cursor result = rdb.query(false, TABLE_ENTRIES, new String[] { "_id",
			"file_id", "entry_id", "translation_id" }, String.format(
			"file_id=%d AND entry_id='%s' AND translation_id='%s'", fileId,
			entryId, translationId), null, null, null, null, "1");

		if (result == null)
		{
			rdb.close();
			return rowid;
		}

		if (result.getCount() == 0)
		{
			rdb.close();
			return insertEntry(new EntryModel(0, fileId, entryId, translationId, 0,
				mTestType));
		}

		result.moveToFirst();
		rowid = result.getLong(0);
		rdb.close();
		result.close();
		return rowid;
	}

	public EntryModel getEntry(long id)
	{
		SQLiteDatabase rdb = null;

		try
		{
			rdb = getReadableDatabase();
		}
		catch (Exception e)
		{
			if (rdb != null)
				rdb.close();
			return null;
		}

		Cursor result = rdb.query(false, TABLE_ENTRIES, EntryModel.fields,
			String.format("_id=%d", id), null, null, null, null, "1");

		if (result == null || result.getCount() == 0)
		{
			if (result != null)
				result.close();
			return null;
		}

		result.moveToFirst();
		EntryModel ret = (EntryModel) (new EntryModel()).populate(result);
		result.close();
		rdb.close();
		return ret;
	}

	public boolean updateEntry(EntryModel entry)
	{
		SQLiteDatabase wdb = null;
		try
		{
			wdb = getWritableDatabase();
		}
		catch (Exception e)
		{
			if (wdb != null)
				wdb.close();
			return false;
		}
		boolean ret = 1 < wdb.update(TABLE_ENTRIES, entry.getValues(),
			String.format("_id=%d", entry.getId()), null);
		wdb.close();
		return ret;
	}

	public boolean addMistake(long fileId, String entryId, String translationId, int testMode)
	{
		long rowid = getEntryId(fileId, entryId, translationId);
		EntryModel entry = getEntry(rowid);
		entry.setMistaken(entry.getMistaken() + 1);
		entry.setTestMode(testMode);
		return updateEntry(entry);
	}

	public HashMap<String, Kvtml.Entry> getTopMistaken(long fileId, int limit)
	{
		SQLiteDatabase rdb = null;

		try
		{
			rdb = getReadableDatabase();
		}
		catch (Exception e)
		{
			if (rdb != null)
				rdb.close();
			return null;
		}

		String orderBy = "mistaken DESC";

		Cursor result = rdb.query(false, TABLE_ENTRIES, EntryModel.fields,
			String.format("file_id=%d", fileId), null, null, null, orderBy,
			String.valueOf(limit));

		if (result == null || result.getCount() < 1)
		{
			if (result != null)
				result.close();
			rdb.close();
			return null;
		}

		HashMap<String, Kvtml.Entry> ret = new HashMap<String, Kvtml.Entry>();
		String entryId = "";
		result.moveToFirst();

		try
		{
			do
			{
				entryId = result.getString(2);
				ret.put(entryId, Config.lastData.entries.get(entryId));
				result.moveToNext();
			} while (!result.isLast());
			result.close();
			rdb.close();
		}
		catch (Exception e)
		{
			if (result != null)
				result.close();
			if (rdb != null)
				rdb.close();
		}
		return ret;
	}

	/**
	 * Checks if the given file has stat entries.
	 *
	 * @param fileId The id of the file record in the "files" table
	 * @return
	 */
	public boolean hasStats(long fileId)
	{
		boolean ret = false;
		SQLiteDatabase rdb = null;
		Cursor result = null;
		try
		{
			rdb = getReadableDatabase();
			result = rdb.query(false, TABLE_ENTRIES, EntryModel.fields,
				String.format("file_id=%d", fileId), null, null, null, null, null);
			ret = (result != null && 0 < result.getCount());
			result.close();
			rdb.close();
		}
		catch (Exception e)
		{
			if (result != null)
				result.close();
			if (rdb != null)
				rdb.close();
			return false;
		}
		return ret;
	}

	/**
	 * @see #hasStats(long)
	 */
	public boolean hasStats(String filename)
	{
		final long fileId = getFileId(filename);
		if (fileId < 1)
			return false;
		return hasStats(fileId);
	}

	/**
	 * Checks if there are any saved stats.
	 *
	 * @return
	 */
	public boolean hasStats()
	{
		boolean ret = false;
		SQLiteDatabase rdb = null;
		Cursor result = null;
		try
		{
			rdb = getReadableDatabase();
			result = rdb.query(false, TABLE_ENTRIES, EntryModel.fields,
				null, null, null, null, null, null);
			ret = (result != null && 0 < result.getCount());
			result.close();
			rdb.close();
		}
		catch (Exception e)
		{
			if (result != null)
				result.close();
			if (rdb != null)
				rdb.close();
			return false;
		}
		return ret;
	}

	public boolean clearFileStats(long fileId)
	{
		SQLiteDatabase wdb = null;
		try
		{
			wdb = getWritableDatabase();
			wdb.execSQL(String.format("DELETE FROM files WHERE _id=%d", fileId));
			wdb
				.execSQL(String.format("DELETE FROM entries WHERE file_id=%d", fileId));
			wdb.close();
		}
		catch (Exception e)
		{
			if (wdb != null)
				wdb.close();
			return false;
		}
		return true;
	}

	public boolean clearAllStats()
	{
		SQLiteDatabase wdb = null;
		try
		{
			wdb = getWritableDatabase();
			wdb.execSQL("DELETE FROM files");
			wdb.execSQL("DELETE FROM entries");
			wdb.close();
		}
		catch (Exception e)
		{
			if (wdb != null)
				wdb.close();
			return false;
		}
		return true;
	}
}