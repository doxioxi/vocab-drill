package com.ekezet.vocabdrill.helpers;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public final class Gradient
{
	public final static int GRADIENT = 0;
	public final static int BRIGHTNESS = 1;

	public static final int DARK_TEXT = 0xc8000000;
	public static final int BRIGHT_TEXT = 0xfcffffff;

	public static final int sBrightnessThreshold = 200;

	private static int sDebug = 0;
	private static int[][][] sCache;

	/**
	 * Makes and returns an RGB gradient based on the sine function.
	 *
	 * @param steps Iteration count (must be greater than zero)
	 * @param amp Amplitude
	 * @param mid Middle
	 * @param freq Frequency
	 * @param phasr Phase of Red
	 * @param phasg Phase of Green
	 * @param phasb Phase of Blue
	 */
	public static int[] make(final int steps, final int amp, final int mid, final float freq,
		final float phasr, final float phasg, final float phasb)
	{
		int[] colors = new int[steps];
		int i = 0, c = 0;
		int r, g, b;
		float RAD = ((float) Math.PI) / 2f;
		float step = RAD / (float) steps;

		if (0 < sDebug)
		{
			Log.d(Gradient.class.getSimpleName(),
				"make(steps, " + String.valueOf(amp) + ", " + String.valueOf(mid)
					+ ", " + String.valueOf(freq) + "f,  " + String.valueOf(phasr)
					+ "f,  " + String.valueOf(phasg) + "f,  " + String.valueOf(phasb)
					+ "f);");
		}

		for (float rad = 0; rad < RAD && i < steps; rad += step)
		{
			// red
			r = (int) Math.round(Math.sin(rad * freq + phasr) * amp + mid);
			if (255 < r)
				r = 255;
			else if (r < 0)
				r = 0;
			// green
			g = (int) Math.round(Math.sin(rad * freq + phasg) * amp + mid);
			if (255 < g)
				g = 255;
			else if (g < 0)
				g = 0;
			// blue
			b = (int) Math.round(Math.sin(rad * freq + phasb) * amp + mid);
			if (255 < b)
				b = 255;
			else if (b < 0)
				b = 0;
			// composite
			c = Color.argb(255, r, g, b);
			if (1 < sDebug)
			{
				Log.d(
					"GRADIENT",
					"color #" + String.valueOf(i) + ": #"
						+ String.format("%x%x%x", r, g, b) + " (" + String.valueOf(r)
						+ ", " + String.valueOf(g) + ", " + String.valueOf(b) + ")");
			}
			colors[i++] = c;
		}
		return colors;
	}

	/**
	 * Default gray shades.
	 */
	public static int[] prototype(final int steps)
	{
		return make(steps, 127, 128, 1f, 0f, 0f, 0f);
	}

	/**
	 * Runtime colours.
	 */
	public static int[] version(final int steps)
	{
		return make(steps, 231, 121, 1.0990003f, -0.9690008f, -0.5090008f, 0.0f);
	}

	/*
	public static int[] traversion(int steps)
	{
		return make(steps, 127, 128, 1.0f, 1.5399989f, 0.10000006f, -0.44999984f);
	}
	*/

	public static int getBrightness(final int color)
	{
		int r = Color.red(color), g = Color.green(color), b = Color.blue(color);
		//int ret = (int) Math.sqrt(r * r * 0.241f + g * g * 0.691f + b * b * 0.068f);
		int ret = (int) Math.sqrt(r * r * 0.2126f + g * g * 0.7152f + b * b * 0.0722f);
		return ret;
	}

	public static int[] getBrightnessArray(final int[] colors)
	{
		int[] ret = new int[colors.length];
		for (int n = 0, N = colors.length; n < N; n++)
			ret[n] = getBrightness(colors[n]);
		return ret;
	}

	public static void buildCache(final int steps)
	{
		if (sCache != null && steps <= sCache.length)
			return;
		int[][][] tmp = new int[steps][][];
		for (int n = 0; n < steps; n++)
		{
			tmp[n] = new int[2][];
			// +1 because cannot be zero
			tmp[n][GRADIENT] = version(n + 1);
			tmp[n][BRIGHTNESS] = getBrightnessArray(tmp[n][GRADIENT]);
		}
		sCache = tmp;
	}

	public static void clearCache()
	{
		sCache = null;
	}

	public static int getColor(final int steps, final int type, final int idx)
	{
		buildCache(steps);
		return sCache[steps - 1][type][idx];
	}

	public static ColorDrawable getColorDrawable(final int steps, final int type, final int idx)
	{
		final int color = sCache[steps - 1][type][idx];
		return new ColorDrawable(color);
	}

	/**
	 * @param offset Start index in the colors array
	 */
	@SuppressWarnings("deprecation")
	public static void colorize(final List<View> views, final int offset)
	{
		final int steps = views.size();
		buildCache(steps + offset);
		final int[][] colors = sCache[(steps + offset) - 1];
		View v;
		StateListDrawable stlist;
		int btnbg;
		int[] fgcolors = new int[steps + offset];
		ColorDrawable activebgcolor;

		for (int i = 0; i < steps; i++)
		{
			v = views.get(i);
			btnbg = colors[GRADIENT][i + offset];
			fgcolors[i] = getReadableColorByColor(btnbg);
			if (0 < (i + offset))
				activebgcolor = new ColorDrawable(colors[GRADIENT][(i + offset) - 1]);
            else
                activebgcolor = new ColorDrawable(Color.TRANSPARENT);

			stlist = new StateListDrawable();
			stlist.addState(new int[] { android.R.attr.state_selected }, activebgcolor);
			stlist.addState(new int[] { android.R.attr.state_pressed }, activebgcolor);
			stlist.addState(new int[] {}, new ColorDrawable(btnbg));
			v.setBackgroundDrawable(stlist);
			if (0 < i && v instanceof TextView)
				((TextView) v).setTextColor(new ColorStateList(new int[][] {
					new int[] { android.R.attr.state_selected },
					new int[] { android.R.attr.state_pressed },
					new int[0],
				}, new int[] {
					fgcolors[i - 1],
					fgcolors[i - 1],
					fgcolors[i],
				}));
		}
	}

	public static void colorize(final List<View> views)
	{
		colorize(views, 0);
	}

	public static int getReadableColorByColor(final int color)
	{
		if (getBrightness(color) < sBrightnessThreshold)
			return BRIGHT_TEXT;
		else
			return DARK_TEXT;
	}

	public static int getReadableColor(final int brightness)
	{
		if (brightness < sBrightnessThreshold)
			return BRIGHT_TEXT;
		else
			return DARK_TEXT;
	}

	public static int debug()
	{
		return sDebug;
	}

	public static void debug(final int level)
	{
		sDebug = level;
	}
}
