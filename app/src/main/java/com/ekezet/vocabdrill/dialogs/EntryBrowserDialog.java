package com.ekezet.vocabdrill.dialogs;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter.FilterListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.activities.SynonymsActivity;
import com.ekezet.vocabdrill.adapters.EntryListAdapter;
import com.ekezet.vocabdrill.kvtml.Kvtml;


public class EntryBrowserDialog extends ListActivity implements AdapterView.OnItemClickListener
{
	public static final int LANGUAGE_DIALOG = 1000;

	/**
	 * Caller Activity sets this.
	 */
	public static Kvtml data = null;
	/**
	 * Caller Activity sets this.
	 */
	private String mWindowTitle = "";

	private String mDisplayLanguageId = "";
	private EditText mEditTextFilter = null;
	private TextView mResultsNumText = null;
	private static EntryListAdapter sAdapter = null;
	private String mFilterText = null;
	private FilterListener mFilterListener;

	/**
	 * Sort entries alphabetically if True.
	 */
	private boolean mDoSort = false;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN,
			LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.dialog_entry_browser);

		Intent i = getIntent();
		if (i != null)
		{
			mDoSort = i.getBooleanExtra("doSort", mDoSort);
			mWindowTitle = i.getStringExtra("windowTitle") != null ? i.getStringExtra("windowTitle") : mWindowTitle;
		}

		setTitle(mWindowTitle);
		mDisplayLanguageId = Config.getQuestionLangId();
		if (mDisplayLanguageId == null)
		{
			Toast.makeText(this, R.string.error_wrong_kvtml_format, Toast.LENGTH_LONG)
				.show();
			finish();
			return;
		}
		loadEntries();

		ListView list = (ListView) findViewById(android.R.id.list);
		list.setTextFilterEnabled(true);
		list.setOnItemClickListener(this);
		//Log.i("Debug","Footer Items Anz=" + list.getFooterViewsCount());

		mResultsNumText = (TextView) findViewById(R.id.text_results_num);
		mFilterListener = new FilterListener()
		{
			@Override
			public void onFilterComplete(int count)
			{
				if (mFilterText != null)
				{
					int ct = sAdapter.getCount();
					mResultsNumText.setText(Html.fromHtml(String.format("<b>Results: </b>%d", ct)));
					mResultsNumText.setVisibility(View.VISIBLE);
				} else
					mResultsNumText.setVisibility(View.GONE);
			}
		};

		mEditTextFilter = (EditText) findViewById(R.id.text_filter);
		mEditTextFilter.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (s == null || s.toString().trim().length() == 0)
					mFilterText = null;
				else
					mFilterText = s.toString();
				sAdapter.getFilter().filter(mFilterText, mFilterListener);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s)
			{
			}
		});
	}

	public void onItemClick(AdapterView<?> l, View v, int position, long id) {

		Kvtml.Entry selectedEntry = (Kvtml.Entry) l.getItemAtPosition(position);
		String entryId = selectedEntry.id;
		HashSet syns = giveSynonyms(entryId);

		List<Kvtml.Entry> synsForEntry = new ArrayList<Kvtml.Entry>();

		if ( syns==null )
		{
			//Log.i("Debug", "No Synonyms defined for selected item mit id=" + entryId);
			Toast toast = Toast.makeText(this,R.string.no_syns_for_entry, Toast.LENGTH_SHORT);
			toast.show();
		}
		else {
			Iterator<String> it = syns.iterator();
			while(it.hasNext()){
				String synId=it.next();
			//	Log.i("Debug", data.entries.get(synId).translations.get(mDisplayLanguageId).text);
				synsForEntry.add(data.entries.get(synId));
			}

			SynonymsActivity.data = data;
			SynonymsActivity.synsForEntry = synsForEntry;
			SynonymsActivity.clickedEntry = selectedEntry;
			SynonymsActivity.mDisplayLanguageId = mDisplayLanguageId;
			SynonymsActivity.mLanguageId = Config.getAnswerLangId();
			final Intent intent = new Intent(this,
					SynonymsActivity.class);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.browser_menu, menu);
		return true;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.option_swap_languages:
				showDialog(LANGUAGE_DIALOG);
				return true;
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id)
	{
		if (data == null)
			return super.onCreateDialog(id);

		Dialog dialog = null;
		switch (id)
		{
			case LANGUAGE_DIALOG:
				final CharSequence[] items = new CharSequence[data.identifiers.size()];
				Iterator<Entry<String, Kvtml.Identifier>> it = data.identifiers
					.entrySet().iterator();
				int n = 0;

				while (it.hasNext())
				{
					items[n] = it.next().getValue().name;
					n++;
				}

				dialog = new AlertDialog.Builder(this).setSingleChoiceItems(items,
					getIdPosition(mDisplayLanguageId),
					new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							mDisplayLanguageId = EntryBrowserDialog.this
								.getIdByPosition(which);
							loadEntries();
							dialog.dismiss();
						}
					}).create();
				break;
		}
		return dialog;
	}

	private void loadEntries()
	{
		if (data == null)
		{
			finish();
			return;
		}

		ArrayList<Kvtml.Entry> entries = null;
		if (mDoSort)
		{
			entries = getSortedEntries(data.entries);
			// sort kills doubles
			// Log.i("Debug","Size="+ entries.size() + "<" + data.entries.size());
		}
		else
		{
			entries = new ArrayList<Kvtml.Entry>();
			entries.addAll(data.entries.values());
		}

		if (entries == null)
		{
			Toast.makeText(this, R.string.error_loading_entries, Toast.LENGTH_LONG)
				.show();
			finish();
			return;
		}

		HashMap<String, Kvtml.Identifier> ids = data.identifiers;
		if (ids == null)
		{
			Toast.makeText(this, R.string.error_loading_identifiers, Toast.LENGTH_LONG)
				.show();
			finish();
			return;
		}

		String idName = ids.get(mDisplayLanguageId).name;
		if (idName == null)
		{
			Toast.makeText(this, R.string.error_loading_identifier, Toast.LENGTH_LONG)
				.show();
			finish();
			return;
		}

		Iterator<Kvtml.Entry> it = entries.iterator();
		setTitle(String.format("%s (%s [#%d])", mWindowTitle, idName, data.entries.size()));
		List<Kvtml.Entry> listItems = new ArrayList<Kvtml.Entry>();
		while (it.hasNext())
			listItems.add(it.next());

		sAdapter = new EntryListAdapter(this, R.id.rowtext, listItems, mDisplayLanguageId);
		setListAdapter(sAdapter);
	}

	private ArrayList<Kvtml.Entry> getSortedEntries(HashMap<String, Kvtml.Entry> entries)
	{
		if (data == null)
			return null;

		SortedSet<Kvtml.Entry> sortedSet = null;
		Object[] sortedArray = null;

		try
		{
			sortedSet = new TreeSet<Kvtml.Entry>(new Comparator<Kvtml.Entry>()
			{
				@Override
				public int compare(Kvtml.Entry lhs, Kvtml.Entry rhs)
				{
					final Kvtml.Translation t = lhs.translations.get(mDisplayLanguageId);
					final String s1 = String.valueOf(t.text);
					final String s2 = String.valueOf(rhs.translations.get(mDisplayLanguageId).text);
					return s1.compareToIgnoreCase(s2);
				}
			});

			ArrayList<Kvtml.Entry> values = new ArrayList<Kvtml.Entry>(
				entries.values());
			sortedSet.addAll(values);
			sortedArray = sortedSet.toArray();
		}
		catch (Exception e)
		{
			return null;
		}

		if (sortedArray == null)
			return null;

		ArrayList<Kvtml.Entry> sortedEntries = new ArrayList<Kvtml.Entry>();
		Kvtml.Entry entry = null;

		for (int n = 0, mn = sortedArray.length; n < mn; n++)
		{
			entry = (Kvtml.Entry) sortedArray[n];
			if (entry == null)
				// XXX: must be reviewed: why not continue here?
				return null;
			sortedEntries.add(entry);
		}

		return sortedEntries;
	}

	public int getIdPosition(String id)
	{
		if (data == null)
			return -1;

		Iterator<String> it = data.identifiers.keySet().iterator();
		int n = 0;

		while (it.hasNext())
		{
			if (String.valueOf(it.next()).equals(id))
				return n;
			n++;
		}

		return -1;
	}

	public String getIdByPosition(int index)
	{
		if (data == null)
			return null;

		Iterator<Entry<String, Kvtml.Identifier>> it = data.identifiers.entrySet()
			.iterator();
		Kvtml.Identifier id = null;
		int n = 0;

		while (it.hasNext())
		{
			id = it.next().getValue();
			if (n == index)
				return id.id;
			n++;
		}

		return Config.getQuestionLangId(data.identifiers);
	}


	public static HashSet<String> giveSynonyms(String id)
	{
		HashSet<String> synSet;
		synSet = new HashSet<String>();
		Iterator<Kvtml.Pair> it = data.synonyms.iterator();
		while(it.hasNext())
		{
			Kvtml.Pair aktPair = it.next();

			if (id.equals(aktPair.firstId))
				synSet.add(aktPair.secondId);
			else if (id.equals(aktPair.secondId))
				synSet.add(aktPair.firstId);
		}
		if (synSet.isEmpty())
			return null;
		return synSet;
	}
}