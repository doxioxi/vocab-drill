package com.ekezet.vocabdrill.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.adapters.FileListAdapter;

public abstract class OpenDialog extends ListActivity
{
	// protected static String PREF_BROWSER_DEFAULT_DIR = "browser_default_dir";
	public static final String INTENT_PATH_PARAMETER = "path";
	public static final String INTENT_NAME_PARAMETER = "name";
	public static final String INTENT_PARENT_PARAMETER = "parent";

	protected String mLastPathPrefName = null;

	/**
	 * Represents an entry in a picker dialog.
	 */
	public abstract class DialogEntry implements Comparable<DialogEntry>
	{
		protected String mPath;

		public DialogEntry(String path)
		{
			mPath = path;
		}

		public String getPath()
		{
			return mPath;
		}

		public boolean canRead()
		{
			return true;
		}

		public abstract String getName();
		public abstract String getParent();
		public abstract DialogEntry[] getFileList();
		public abstract DialogEntry[] getFolderList();
		public abstract boolean isDirectory();
		abstract protected boolean isSelected();
		abstract protected boolean exists();

		@Override
		public int compareTo(DialogEntry another)
		{
			return mPath.compareTo(another.getPath());
		}
	}

	protected SharedPreferences mPrefs;

	protected DialogEntry mCurrentEntry = null;
	protected String mDefaultPath = null;
	protected String mStoragePath = null;
	protected String mSelectedFile = null;
	protected boolean mEnableDirScan = true;
	protected List<DialogEntry> mPaths = null;

	private FileListAdapter mAdapter;
	private TextView mTxtPath = null;
	private TextView mTxtGoUp = null;

	/**
	 * Initialize File Open dialog.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// enter fullscreen mode (hide status bar)
		getWindow()
			.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.dialog_file_open);

		// create default objects
		mLastPathPrefName = getLastPathPrefName();
		mPrefs = Config.getPrefs();
		mTxtPath = (TextView) findViewById(R.id.currentPath);
		mTxtGoUp = (TextView) findViewById(R.id.go_to_parent_directory);
		mAdapter = new FileListAdapter(this, R.layout.item_file_list);
		mAdapter.setParentDirLabel(getResources().getString(R.string.go_to_parent_directory));
		mPaths = new ArrayList<DialogEntry>();

		initialize(savedInstanceState);
		updateList(mCurrentEntry);
		setListAdapter(mAdapter);
	}

	/**
	 * Updates the list with the contents of a folder (no recursion).
	 *
	 * @param path to the folder
	 */
	protected void updateList(DialogEntry entry)
	{
		if (entry == null)
			return;
		// update path display
		mCurrentEntry = entry;
		String path = entry.getPath();
		if (mStoragePath != null)
			mTxtPath.setText(path.replace(mStoragePath, Config.EXT_STORAGE_ALIAS));
		else
			mTxtPath.setText(path);
		// clear lists
		mPaths.clear();
		mAdapter.clear();

			// add "up..." item if not in root
			if (path.equals("/"))
				mTxtGoUp.setVisibility(View.GONE);
			else
				mTxtGoUp.setVisibility(View.VISIBLE);

		// get directory list
		DialogEntry[] folders = entry.getFolderList();
		if (folders != null && 0 < folders.length)
		{
			Arrays.sort(folders);
			if (Config.scanDirectories && mEnableDirScan)
			{
				// show number of files in folder if enabled,
				// XXX: This may be slow...
				DialogEntry folder = null;
				for (int i = 0, I = folders.length; i < I; i++)
				{
					folder = folders[i];
					DialogEntry[] list = folder.getFileList();
					if (list != null && 0 < list.length)
						mAdapter.add(String.format("%s (%d)%s", folder.getName(), list.length, File.separator));
					else
						mAdapter.add(String.format("%s%s", folder.getName(), File.separator));
					mPaths.add(folder);
				}
			} else
			{
				DialogEntry folder = null;
				for (int i = 0, I = folders.length; i < I; i++)
				{
					folder = folders[i];
					mAdapter.add(String.format("%s%s", folder.getName(), File.separator));
					mPaths.add(folder);
				}
			}
		}
		// sort and list files
		DialogEntry[] files = entry.getFileList();
		if (files == null || files.length == 0)
			return;
		Arrays.sort(files);
		DialogEntry file = null;
		for (int i = 0, I = files.length; i < I; i++)
		{
			file = files[i];
			mAdapter.add(file.getName());
			mPaths.add(file);
		}
	}

	/**
	 * Good place for extending classes to set path variables.
	 *
	 * @param savedInstanceState
	 */
	protected void initialize(Bundle savedInstanceState)
	{
		// mStoragePath = "";
		// mDefaultPath = "";
		// mCurrentPath = "";
	}

	/**
	 * Called when a list item is selected.
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		final DialogEntry selectedItem = mPaths.get(position);

		if (selectedItem.isSelected())
		{
			// pass result back to caller
			final Intent data = new Intent();
			data.putExtra(INTENT_PATH_PARAMETER, selectedItem.getPath());
			data.putExtra(INTENT_NAME_PARAMETER, selectedItem.getName());
			data.putExtra(INTENT_PARENT_PARAMETER, selectedItem.getParent());
			String lastPath = selectedItem.getParent();
			final SharedPreferences.Editor editor = mPrefs.edit();
			editor
				.putString(mLastPathPrefName, lastPath)
				.commit();
			setResult(Activity.RESULT_OK, data);
			finish();
			return;
		}

		// verify read permission
		if (selectedItem.canRead())
			updateList(selectedItem);
		else
		{
			String errorText = getResources().getString(R.string.error_cannot_read_folder);
			Toast.makeText(this, errorText, Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Save configuration on pause.
	 */
	@Override
	protected void onPause()
	{
		super.onPause();
		if (mCurrentEntry == null)
			return;
		final SharedPreferences.Editor editor = mPrefs.edit();
		editor
			.putString(mLastPathPrefName, mCurrentEntry.getPath())
			.commit();
	}

	/**
	 * Save instance state.
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putString(mLastPathPrefName, mCurrentEntry.getPath());
	}

	public void onGoUpClick(View view)
	{
		final DialogEntry parent = newEntry(mCurrentEntry.getParent());
		if (parent == null)
			return;
		updateList(parent);
	}

	protected abstract String getLastPathPrefName();

	abstract protected DialogEntry newEntry(String path);
}
