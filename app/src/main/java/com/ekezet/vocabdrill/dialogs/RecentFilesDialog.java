package com.ekezet.vocabdrill.dialogs;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.adapters.RecentFilesAdapter;

import java.io.File;
import java.util.HashMap;

public class RecentFilesDialog extends ListActivity
{
	protected String mExtDir = null;
	protected String mCacheDir = null;

	public abstract class RecentItem
	{
		protected String mOrigPath = null;
		protected String mPath = null;
		protected String mProto = null;

		public RecentItem(String path, String proto)
		{
			mOrigPath = path;
			mProto = proto;
			/**
			 * XXX: FUN FACT: This didn't work on 2.1 device:
			 *   mPath = path.replace(proto != null ? proto : "", "")
			 * Tested on device: XPeria X10 mini pro
			 */
			if (proto != null)
				path = path.replace(proto, "");
			mPath = path;
		}

		public String getPath()
		{
			return mPath;
		}

		public String getLocalPath()
		{
			return mPath;
		}

		public String getOrigPath()
		{
			return mOrigPath;
		}

		public String getProto()
		{
			return mProto;
		}

		public String getOrigParent()
		{
			return (null == mProto ? "" : mProto) + getParent();
		}

		public abstract String getName();

		public abstract String getParent();

		public abstract String getDisplayPath();
	}

	public class RecentFile extends RecentItem
	{
		protected File mFile;

		public RecentFile(String path, String proto)
		{
			super(path, proto);
			mFile = new File(mPath);
		}

		public RecentFile(String path)
		{
			super(path, null);
			mFile = new File(mPath);
		}

		@Override
		public String getName()
		{
			return mFile.getName();
		}

		@Override
		public String getParent()
		{
			return mFile.getParent();
		}

		@Override
		public String getDisplayPath()
		{
			String s = getLocalPath().replaceAll(mExtDir, Config.EXT_STORAGE_ALIAS);
			Log.d(getClass().getSimpleName(), "display (ext): " + s);
			return s;
		}
	}

	public class DbxRecentFile extends RecentFile
	{
		public DbxRecentFile(String path)
		{
			super(path);
			mFile = new File(Config.getDbxCacheDir(), mPath);
		}

		@Override
		public String getDisplayPath()
		{
			String s = getLocalPath().replaceAll(mCacheDir, Config.CACHE_DIR_ALIAS);
			Log.d(getClass().getSimpleName(), "display (cache): " + s);
			return s;
		}
	}

	private static HashMap<String, RecentItem> sList = null;

	private RecentFilesAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow()
			.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		mExtDir = Environment
			.getExternalStorageDirectory()
			.getAbsolutePath();
		mCacheDir = Config.getDbxCacheDir().getAbsolutePath();

		sList = new HashMap<String, RecentItem>();
		mAdapter = new RecentFilesAdapter(this, R.layout.item_recent_file);
		updateList();
		setListAdapter(mAdapter);
	}

	private void updateList()
	{
		mAdapter.clear();
		RecentItem entry = null;
		String path = null;
		for (int n = 0, N = Config.recentFilesList.length; n < N; n++)
		{
			path = Config.recentFilesList[n];
			if (path.length() == 0)
				continue;
			entry = createRecentFile(path);
			mAdapter.add(entry);
		}
	}

	private RecentItem createRecentFile(String path)
	{
		if (sList.containsKey(path))
			return sList.get(path);

		String dbxCachePath = Config.getDbxCacheDir().getAbsolutePath();
		RecentItem ret = null;
		if (path.startsWith(dbxCachePath))
			ret = new DbxRecentFile(path);
		else
			ret = new RecentFile(path);
		sList.put(path, ret);
		return ret;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.recent_files_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.menu_clear:
				Config.clearRecentFilesList();
				finish();
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		final Intent data = new Intent();
		final RecentItem item = createRecentFile(Config.recentFilesList[position]);
		data.setData(Uri.parse(item.getLocalPath()));
		setResult(Activity.RESULT_OK, data);
		finish();
	}
}
