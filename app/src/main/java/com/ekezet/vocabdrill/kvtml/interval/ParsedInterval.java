package com.ekezet.vocabdrill.kvtml.interval;

import java.text.ParseException;
import java.util.Calendar;

public final class ParsedInterval
{
	private int[] mValues;

	public ParsedInterval(String interval)
	{
		mValues = new int[3];
		set(interval);
	}

	public ParsedInterval(int[] interval)
	{
		mValues = new int[3];
		set(interval);
	}

	public void set(String interval)
	{
		final String[] parts = interval.split(":");
		for (int i = 0, I = mValues.length, I2 = parts.length; i < I; i++)
		{
			if (i < I2)
				mValues[i] = Integer.parseInt(parts[i]);
			else
				mValues[i] = 0;
		}
		return;
	}

	public void set(int[] values)
	{
		if (values.length < 3)
			return;
		mValues[0] = values[0];
		mValues[1] = values[1];
		mValues[2] = values[2];
	}

	public void set(DateItem part, int value)
	{
		switch (part)
		{
			case MONTHS:
				mValues[0] = value;
				break;
			case DAYS:
				mValues[1] = value;
				break;
			case HOURS:
				mValues[2] = value;
				break;
		}
	}

	public int[] get()
	{
		return mValues;
	}

	public int getPart(DateItem part)
	{
		switch (part)
		{
			case MONTHS:
				return mValues[0];
			case DAYS:
				return mValues[1];
			case HOURS:
				return mValues[2];
		}
		return 0;
	}

	public String partToString(DateItem part)
	{
		int n = 0;
		switch (part)
		{
			case MONTHS:
				n = mValues[0];
				break;
			case DAYS:
				n = mValues[1];
				break;
			case HOURS:
				n = mValues[2];
				break;
		}
		return String.format("%02d", n);
	}

	/**
	 * Parses an interval string into a Calendar object.
	 *
	 * @param interval
	 * @throws ParseException
	 */
	public Calendar toCalendar() throws ParseException
	{
		// parse String according to Config.INTERVAL_DATE_FORMAT
		// XXX: Config.INTERVAL_DATE_FORMAT should somehow be referenced here
		Calendar cal = Calendar.getInstance();
		// increase day of month by one, as \delta day = 0 can not be stored
		cal.set(0, mValues[0],
			mValues[1] + 1,
			mValues[2], 0, 0);
		return cal;
	}

	public String toString()
	{
		return String.format("%02d:%02d:%02d", mValues[0], mValues[1], mValues[2]);
	}
}