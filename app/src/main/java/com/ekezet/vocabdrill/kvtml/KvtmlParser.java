package com.ekezet.vocabdrill.kvtml;

import android.util.Log;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Stack;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

final public class KvtmlParser
{
	/**
	 * XmlPullParser instance.
	 */
	private XmlPullParser mParser;

	/**
	 * Kvtml object containing the parser output.
	 */
	private Kvtml mKvtml;

	/**
	 * Input stream reader.
	 */
	private KvtmlFileReader mFileReader;

	/**
	 * Path to the input KVTML file.
	 */
	private String mFilename;

	/**
	 * Information section parser.
	 */
	final class InformationTagParser extends KvtmlTagParser
	{
		@Override
		public void parse() throws XmlPullParserException, IOException
		{
			int event = 0;
			boolean done = false;
			String tag = "";
			final DateFormat df = new SimpleDateFormat("yyyy-MM-dd",
				Locale.getDefault());

			do
			{
				event = mParser.next();
				tag = mParser.getName();

				if (tag != null)
					tag = tag.toLowerCase(Locale.getDefault());

				switch (event)
				{
					case XmlPullParser.START_TAG:
						if (Kvtml.GENERATOR_TAG.equals(tag))
							mKvtml.generator = mParser.nextText();
						else if (Kvtml.TITLE_TAG.equals(tag))
							mKvtml.title = mParser.nextText();
						else if (Kvtml.DATE_TAG.equals(tag))
							try
							{
								mKvtml.date = df.parse(mParser.nextText());
							}
							catch (ParseException e)
							{
							}
						break;

					case XmlPullParser.END_TAG:
						if (Kvtml.INFORMATION_TAG.equals(tag))
							done = true;
						break;
				}
			} while (!done);
		}
	}

	/**
	 * Identifiers section parser.
	 */
	final class IdentifiersTagParser extends KvtmlTagParser
	{
		@Override
		void parse() throws XmlPullParserException, IOException
		{
			int event = 0;
			boolean done = false;
			String tag = "";
			Kvtml.Identifier identifier = null;

			do
			{
				event = mParser.next();
				tag = mParser.getName();

				if (tag != null)
					tag = tag.toLowerCase(Locale.getDefault());

				switch (event)
				{
					case XmlPullParser.START_TAG:
						if (Kvtml.IDENTIFIER_TAG.equals(tag))
						{
							identifier = mKvtml.newIdentifier();
							identifier.id = mParser.getAttributeValue(null, "id");
						} else if (Kvtml.NAME_TAG.equals(tag))
							identifier.name = mParser.nextText();
						else if (Kvtml.LOCALE_TAG.equals(tag))
							identifier.locale = mParser.nextText();
						break;

					case XmlPullParser.END_TAG:
						if (Kvtml.IDENTIFIER_TAG.equals(tag))
							mKvtml.identifiers.put(identifier.id, identifier);
						else if (Kvtml.IDENTIFIERS_TAG.equals(tag))
							done = true;
						break;
				}
			} while (!done);
		}
	}

	/**
	 * Entries section parser.
	 */
	final class EntriesTagParser extends KvtmlTagParser
	{
		@Override
		void parse() throws XmlPullParserException, IOException
		{
			int event = 0;
			boolean done = false;
			String tag = "";
			Kvtml.Entry entry = null;
			Kvtml.Translation translation = null;
			boolean surroundingGradeTag = false;

			do
			{
				event = mParser.next();
				tag = mParser.getName();
				if (tag != null)
					tag = tag.toLowerCase(Locale.getDefault());

				switch (event)
				{
					case XmlPullParser.START_TAG:
						if (Kvtml.ENTRY_TAG.equals(tag))
						{
							entry = mKvtml.newEntry();
							entry.id = mParser.getAttributeValue(null, "id");
						} else if (Kvtml.TRANSLATION_TAG.equals(tag))
						{
							translation = mKvtml.newTranslation();
							translation.id = mParser.getAttributeValue(null, "id");
						} else if (Kvtml.TEXT_TAG.equals(tag))
							translation.text = mParser.nextText();
						else if (Kvtml.COMMENT_TAG.equals(tag))
							translation.comment = mParser.nextText();
						else if (Kvtml.GRADE_TAG.equals(tag))
							surroundingGradeTag = true;
						else if (surroundingGradeTag && Kvtml.CURRENT_GRADE_TAG.equals(tag))
							entry.currentGrade = Integer.valueOf(mParser.nextText());
						else if (surroundingGradeTag && Kvtml.COUNT_TAG.equals(tag))
							entry.count = Integer.valueOf(mParser.nextText());
						else if (surroundingGradeTag && Kvtml.ERROR_COUNT_TAG.equals(tag))
							entry.errorCount = Integer.valueOf(mParser.nextText());
						else if (surroundingGradeTag && Kvtml.GRADE_DATE_TAG.equals(tag))
							entry.date = mParser.nextText();
						break;

					case XmlPullParser.END_TAG:
						if (Kvtml.TRANSLATION_TAG.equals(tag))
						{
							if (translation.text == null)
								break;
							if (0 == translation.text.length())
								break;
							entry.translations.put(translation.id, translation);
						} else if (Kvtml.ENTRY_TAG.equals(tag))
						{
							if (entry != null)
								if (entry.translations != null && entry.translations.size() > 0)
								{
									mKvtml.entries.put(entry.id, entry);
							/*		if (mKvtml.entries.put(entry.id, entry)==null)
									{
										Log.i("Debug", "put entry id=" + entry.id + " " + entry.translations.get("0").text + " " + entry.translations.get("1").text);
									}
									else
									{
										Log.i("Debug", "no put done");
									}*/
								}
						} else if (Kvtml.ENTRIES_TAG.equals(tag))
							done = true;
						else if (Kvtml.GRADE_TAG.equals(tag))
							surroundingGradeTag = false;
						break;
				}
			} while (!done);
		}
	}

	/**
	 * Lessons section parser.
	 */
	final class LessonsTagParser extends KvtmlTagParser
	{
		@Override
		void parse() throws XmlPullParserException, IOException
		{
			int event = 0;
			boolean done = false;
			String tag = "";
			Kvtml.Lesson lesson = null;
			Kvtml.Lesson parent = null;
			Stack<Kvtml.Lesson> stack = new Stack<Kvtml.Lesson>();

			int depth = 0;
			int id = 0;

			do
			{
				event = mParser.next();
				tag = mParser.getName();
				if (tag != null)
					tag = tag.toLowerCase(Locale.getDefault());

				switch (event)
				{
					case XmlPullParser.START_TAG:
						if (Kvtml.CONTAINER_TAG.equals(tag))
						{
							lesson = mKvtml.newLesson();
							if (parent == null)
							{
								lesson.id = Integer.toString(id);
								id++;
							} else
							{
								lesson.id = Integer.toString(parent.subLessons.size());
							}
							/*
							 * using a stack to handle possible tree structures of
							 * lessons and considering that the entries of a lesson
							 * might get parsed after sublessons are handled
							 */
							if (parent != null)
							{
								stack.push(parent);
							}
							parent = lesson;
							depth++;
						} else if (Kvtml.NAME_TAG.equals(tag))
						{
							lesson.name = mParser.nextText();
						} else if (Kvtml.INPRACTICE_TAG.equals(tag))
							lesson.inpractice = Boolean.valueOf(mParser.nextText());
						else if (Kvtml.ENTRY_TAG.equals(tag))
						{
							lesson.keys.add(mParser.getAttributeValue(null, "id"));
						}
						break;

					case XmlPullParser.END_TAG:
						if (Kvtml.CONTAINER_TAG.equals(tag))
						{
							depth--;
							if (depth == 0)
							{
								mKvtml.lessons.put(lesson.id, lesson);
								parent = null;
							} else
							{
								parent = stack.pop();
								parent.subLessons.put(lesson.id, lesson);
								lesson = parent;
							}
						} else if (Kvtml.LESSONS_TAG.equals(tag))
							done = true;
						break;
				}
			} while (!done);
		}
	}


	final class PairTagParser extends KvtmlTagParser
	{
		@Override
		void parse() throws XmlPullParserException, IOException
		{

			int event = 0;
			boolean done = false;
			boolean leftDone = false;
			String tag = "";
			Kvtml.Pair pair = null;

			do {
				event = mParser.next();
				tag = mParser.getName();
				if (tag != null)
					tag = tag.toLowerCase(Locale.getDefault());

				switch (event)
				{
					case XmlPullParser.START_TAG:
						if (Kvtml.ENTRY_TAG.equals(tag)) {
							if (!leftDone) {
								pair = mKvtml.newPair();
								pair.firstId = mParser.getAttributeValue(null, "id");
							} else
								pair.secondId = mParser.getAttributeValue(null, "id");

						} else if (Kvtml.TRANSLATION_TAG.equals(tag)) {
							if (!leftDone) {
								pair.firstLangId = mParser.getAttributeValue(null, "id");
								leftDone = true;
							} else
								pair.secondLangId = mParser.getAttributeValue(null, "id");
						}
						break;
					case XmlPullParser.END_TAG:
						if (Kvtml.ENTRY_TAG.equals(tag) && !pair.secondLangId.equals(""))
						{
							//Log.i("Synonyme sind", "Eintrag " + pair.firstId + " und Eintrag " + pair.secondId + "in der Sprache " + pair.firstLangId + "=" + pair.secondLangId);
							try { mKvtml.synonyms.add(pair); }
							catch (Exception e)	{ e.printStackTrace(); 	}
							done = true;
						}
						break;
				}

			} while (!done);

		}
	}
	/**
	 * KVTML parser constructor.
	 *
	 * @param filename
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public KvtmlParser(String filename) throws XmlPullParserException,
		IOException
	{
		mFilename = filename;
		mFileReader = new KvtmlFileReader(mFilename);
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		mParser = factory.newPullParser();
		// create an empty KVTMl object for safety
		mKvtml = new Kvtml();
	}

	/**
	 * Main parsing function.
	 *
	 * @return
	 * @throws XmlPullParserException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws IOException
	 */
	public Kvtml parse() throws XmlPullParserException, IllegalAccessException,
		InstantiationException, IOException
	{
		int parserEvent;
		String tag;
		mParser.setInput(mFileReader);
		parserEvent = mParser.next();

		do
		{
			if (parserEvent == XmlPullParser.START_TAG)
			{
				tag = mParser.getName().toLowerCase(Locale.getDefault());
				if (Kvtml.ROOT_TAG.equals(tag))
					mKvtml.version = mParser.getAttributeValue(null, "version");
				else if (Kvtml.INFORMATION_TAG.equals(tag))
					new InformationTagParser();
				else if (Kvtml.IDENTIFIERS_TAG.equals(tag))
					new IdentifiersTagParser();
				else if (Kvtml.ENTRIES_TAG.equals(tag))
					new EntriesTagParser();
				else if (Kvtml.LESSONS_TAG.equals(tag))
					new LessonsTagParser();
				else if (Kvtml.PAIR_TAG.equals(tag))
					new PairTagParser();
			}
			parserEvent = mParser.next();
		} while (parserEvent != XmlPullParser.END_DOCUMENT);

		return mKvtml;
	}

	public KvtmlFileReader getFileReader()
	{
		return mFileReader;
	}

	public Kvtml getParsedDocument()
	{
		return mKvtml;
	}


	// public static void saveEntry(String filename, Kvtml.Entry entry) {
	//
	// BufferedReader reader = null;
	// BufferedWriter writer = null;
	// File sd = Environment.getExternalStorageDirectory();
	// File writeFile = new File(sd, "test_saveEntry.kvtml");
	//
	// try {
	// reader = new BufferedReader(new InputStreamReader(new
	// FileInputStream(filename), KvtmlParser.FILE_ENCODING));
	// writer = new BufferedWriter(new OutputStreamWriter(new
	// FileOutputStream(writeFile), KvtmlParser.FILE_ENCODING));
	// int count = 0;
	// for (String line; (line = reader.readLine()) != null;) {
	// if (count != 179) {
	// writer.write(line);
	// writer.newLine();
	// count++;
	// } else {
	// BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new
	// FileOutputStream(new File(sd, "bla.kvtml")), KvtmlParser.FILE_ENCODING));
	// XmlSerializer s = Xml.newSerializer();
	// s.setOutput(w);
	// s.startTag(null, Kvtml.ENTRY_TAG);
	// s.attribute(null, "id", entry.id);
	// for(Kvtml.Translation trans : entry.translations.values()) {
	// s.startTag(null, Kvtml.TRANSLATION_TAG);
	// s.attribute(null, "id", trans.id);
	//
	// s.startTag(null, Kvtml.TEXT_TAG);
	// s.text(trans.text);
	// s.endTag(null, Kvtml.TEXT_TAG);
	//
	// if(!trans.comment.equals("")) {
	// s.startTag(null, Kvtml.COMMENT_TAG);
	// s.text(trans.comment);
	// s.endTag(null, Kvtml.COMMENT_TAG);
	// }
	// s.endTag(null, Kvtml.TRANSLATION_TAG);
	// }
	// s.endTag(null, Kvtml.ENTRY_TAG);
	// s.flush();
	// w.close();
	// count++;
	// }
	// }
	// } catch (IOException e) {
	// Log.i(getClass().getSimpleName(), e.getClass().getName(), e);
	// } finally {
	// try {
	// writer.close();
	// reader.close();
	// } catch (IOException e) {
	// Log.i(getClass().getSimpleName(), e.getClass().getName(), e);
	// }
	//
	// }
	//
	// }
}