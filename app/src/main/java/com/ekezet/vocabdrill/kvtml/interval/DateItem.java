package com.ekezet.vocabdrill.kvtml.interval;

public enum DateItem
{
	MONTHS, DAYS, HOURS
}
