package com.ekezet.vocabdrill.kvtml;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

final public class Kvtml
{
	final public static String DOCTYPE =
		" kvtml PUBLIC \"kvtml2.dtd\" \"http://edu.kde.org/kvtml/kvtml2.dtd\"";

	final public static String VERSION = "2.0";

	final public static String ROOT_TAG = "kvtml";

	final public static String INFORMATION_TAG = "information";
	final public static String GENERATOR_TAG = "generator";
	final public static String TITLE_TAG = "title";
	final public static String DATE_TAG = "date";

	final public static String IDENTIFIERS_TAG = "identifiers";
	final public static String IDENTIFIER_TAG = "identifier";
	final public static String NAME_TAG = "name";
	final public static String LOCALE_TAG = "locale";

	final public static String ENTRIES_TAG = "entries";
	final public static String ENTRY_TAG = "entry";
	final public static String COMMENT_TAG = "comment";
	final public static String TRANSLATION_TAG = "translation";
	final public static String TEXT_TAG = "text";

	final public static String GRADE_TAG = "grade";
	final public static String CURRENT_GRADE_TAG = "currentgrade";
	final public static String COUNT_TAG = "count";
	final public static String ERROR_COUNT_TAG = "errorcount";
	final public static String GRADE_DATE_TAG = "date";
	final public static String GRADE_DATE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	final public static String LESSONS_TAG = "lessons";
	final public static String CONTAINER_TAG = "container";
	final public static String INPRACTICE_TAG = "inpractice";

	final public static String PAIR_TAG = "pair";


	public class Node
	{
		public String id;
	}

	public class Identifier extends Node
	{
		public String name;
		public String locale;
	}

	public class Translation extends Node
	{
		public String text;
		public String comment = "";
	}

	public class Entry extends Node
	{
		public HashMap<String, Translation> translations;
		public int currentGrade;
		public int count;
		public int errorCount;
		public String date;

		public Entry()
		{
			translations = new HashMap<String, Translation>();
			currentGrade = 0;
			count = 0;
			errorCount = 0;
			date = "";
		}
	}

	public class Lesson extends Node
	{
		public String name;
		public Boolean inpractice;
		public Set<String> keys;
		public HashMap<String, Lesson> subLessons;

		public Lesson()
		{
			keys = new HashSet<String>();
			subLessons = new HashMap<String, Lesson>();
			inpractice=true;
		}
	}

	public class Pair
	{
		public String firstId;
		public String secondId;
		public String firstLangId;
		public String secondLangId;

		public Pair()
		{
			firstId = "";
			secondId = "";
			firstLangId = "";
			secondLangId = "";
		}
	}

	public class HalfPair
	{
		public String idOfEntry;
		public String langId;

		public HalfPair()
		{
			idOfEntry = "";
			langId = "";
		}
	}


	public String generator;
	public String title;
	public Date date;
	public String version;

	public HashMap<String, Identifier> identifiers;
	public HashMap<String, Entry> entries;
	public HashMap<String, Lesson> lessons;
	public HashSet<Pair> synonyms;

	public Kvtml()
	{
		entries = new HashMap<String, Entry>();
		identifiers = new HashMap<String, Identifier>();
		lessons = new HashMap<String, Lesson>();
		synonyms = new HashSet<Pair>();
		version = null;
	}

	public Kvtml.Identifier newIdentifier()
	{
		return new Kvtml.Identifier();
	}

	public Kvtml.Entry newEntry()
	{
		return new Kvtml.Entry();
	}

	public Kvtml.Pair newPair()
	{
		return new Kvtml.Pair();
	}

	public Kvtml.Translation newTranslation()
	{
		return new Kvtml.Translation();
	}

	public Kvtml.Lesson newLesson()
	{
		return new Kvtml.Lesson();
	}


	/**
	 * Clones a KVTML object containing the specified entries. Useful for creating
	 * test documents at runtime.
	 *
	 * @param clonedEntries
	 * @return A new KVTML document object.
	 */
	public Kvtml cloneFromEntries(HashMap<String, Kvtml.Entry> clonedEntries)
	{
		Kvtml clone = new Kvtml();
		clone.version = VERSION;
		clone.generator = new String(generator);
		clone.title = new String(title);
		clone.date = (Date) date.clone();
		clone.identifiers.putAll(identifiers);
		clone.entries.putAll(clonedEntries);
		clone.lessons.putAll(lessons);

		/*Iterator<Pair> it = synonyms.iterator();
		while(it.hasNext()) {
			clone.synonyms.add(it.next());
				}*/
		return clone;
	}
}