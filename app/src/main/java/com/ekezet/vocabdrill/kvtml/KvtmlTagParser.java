package com.ekezet.vocabdrill.kvtml;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

/**
 * Base class for implementing parsers for specific KVTML tags.
 */
public abstract class KvtmlTagParser
{
	public KvtmlTagParser()
	{
		try
		{
			parse();
		}
		catch (Exception e)
		{
		}
	}
	
	abstract void parse() throws XmlPullParserException, IOException;
}
