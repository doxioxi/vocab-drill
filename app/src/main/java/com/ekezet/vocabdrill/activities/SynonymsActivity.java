package com.ekezet.vocabdrill.activities;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.adapters.EntryListAdapter;
import com.ekezet.vocabdrill.dialogs.EntryBrowserDialog;
import com.ekezet.vocabdrill.kvtml.Kvtml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;


public class SynonymsActivity extends ListActivity implements AdapterView.OnItemClickListener
{

    public static Kvtml data = null;
    public static List<Kvtml.Entry> synsForEntry = null;
    public static Kvtml.Entry clickedEntry;
    public static String mDisplayLanguageId = "";
    public static String mLanguageId = "";
    private static EntryListAdapter sAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_synonyms);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_with_button);

        final TextView leftText = (TextView) findViewById(R.id.left_text);
        //final TextView rightText = (TextView) findViewById(R.id.right_text);

        final Button btn = (Button) findViewById(R.id.esc_to_browser);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Jump back to browser
                setResult(1);
                finish();
            }
        });


        leftText.setText("Synonyms (" + synsForEntry.size() + ")");
       // rightText.setText("Right");

        //setTitle("Synonyms (" + synsForEntry.size() + ")");

        /*Iterator<Kvtml.Entry> iter = synsForEntry.iterator();
        while (iter.hasNext()){
            Log.i("Debug","" + iter.next().translations.get("0").text);
                   }*/
        TextView wortView = (TextView) findViewById(R.id.wort1);
        wortView.setText(clickedEntry.translations.get(mDisplayLanguageId).text);
        wortView = (TextView) findViewById(R.id.wort2);
        wortView.setText("   " + clickedEntry.translations.get(mLanguageId).text);

        String standardSynonymLang = "0"; // add this to config menu would be fine

        sAdapter = new EntryListAdapter(this, R.id.rowtext, synsForEntry, standardSynonymLang);
        setListAdapter(sAdapter);
        ListView list = (ListView) findViewById(android.R.id.list);
        list.setOnItemClickListener(this);


        }

    public void onItemClick(AdapterView<?> l, View v, int position, long id)
        {
            clickedEntry = (Kvtml.Entry) l.getItemAtPosition(position);

            HashSet syns = EntryBrowserDialog.giveSynonyms(clickedEntry.id);

            List<Kvtml.Entry> newsynsForEntry = new ArrayList<Kvtml.Entry>();
            synsForEntry = newsynsForEntry;

            if ( syns!=null )
                {
                Iterator<String> it = syns.iterator();
                while (it.hasNext()) {
                    String synId = it.next();
                    synsForEntry.add(data.entries.get(synId));
                }

                final Intent intent = new Intent(this,
                        SynonymsActivity.class);
                startActivityForResult(intent,0);
            }
        }

    // Jump back to browser if synonym of synonym was shown
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ( resultCode==1 )
        {
            setResult(1);
            finish();
        }
    }

    }

