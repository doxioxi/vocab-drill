package com.ekezet.vocabdrill.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.activities.test_types.FlashcardActivity;
import com.ekezet.vocabdrill.activities.test_types.MultipleChoiceActivity;
import com.ekezet.vocabdrill.activities.test_types.QuestionAnswerActivity;
import com.ekezet.vocabdrill.dialogs.EntryBrowserDialog;
import com.ekezet.vocabdrill.helpers.StatsOpenHelper;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.kvtml.KvtmlFileReader;
import com.ekezet.vocabdrill.kvtml.KvtmlFileReader.OnReadingFileListener;
import com.ekezet.vocabdrill.kvtml.KvtmlParser;

import java.io.File;
import java.util.HashMap;

public class VocabFileActivity extends BaseMenuActivity
{
	private static ProgressDialog sProgressDialog;
	private Button mStatsButton;

	/**
	 * Basename of currently opened file.
	 */
	private static String sFilename = "";

	private String mLastFilename = "";
	private String mLoadingText;
	private MenuItem mClearStatsMenu;

	/**
	 * Used to update the progress dialog.
	 */
	private static Handler sProgressHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			sProgressDialog.setProgress(msg.arg1);
		}
	};

	/**
	 * Used to track KVTML file loading progress.
	 */
	private static OnReadingFileListener mOnReadingFileListener = new OnReadingFileListener()
	{
		@Override
		public void onReadingFile(long current, long total)
		{
			final Message msg = sProgressHandler.obtainMessage();
			msg.arg1 = (int) current;
			sProgressHandler.sendMessage(msg);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN,
			LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_vocab_file);
		getSupportActionBar().setHomeButtonEnabled(true);

		/**
		 * Check if we have an intent from outside of the app.
		 */
		Uri data = getIntent().getData();
		if (data != null)
		{
			// trim "//" from beginning
			String pathData = data.getSchemeSpecificPart().substring(2);
			if (0 < pathData.length())
			{
				Log.d(getClass().getSimpleName(), String.format("Loading: '%s'", pathData));
				Config.inputFile = new File(pathData);
				// does input file exist?
				if (!Config.inputFile.exists())
				{
					Toast.makeText(getApplicationContext(), R.string.error_file_not_found, Toast.LENGTH_SHORT).show();
					finish();
					return;
				}
				// normally config is loaded in MainActivity during startup
				Config.load(VocabFileActivity.this);
				Config.insertRecentFile(pathData);
				Config.saveRecentFilesList();
			} else
			{
				Toast.makeText(getApplicationContext(), R.string.error_file_not_found, Toast.LENGTH_SHORT).show();
				finish();
				return;
			}
		}

		final Resources res = getResources();
		mLoadingText = res.getString(R.string.loading_label);
		sProgressDialog = new ProgressDialog(this);
		sProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		sFilename = Config.inputFile.getName();
		loadKvtmlData();
	}

	/**
	 * Loads currently selected KVTML file and displays the progress dialog.
	 */
	private void loadKvtmlData()
	{
		try
		{
			// set title text to loading message
			setTitle(mLoadingText);
			// create the parser and file reader objects
			final KvtmlParser parser = new KvtmlParser(Config.inputFile.getPath());
			mLastFilename = Config.inputFile.getName();
			KvtmlFileReader reader = parser.getFileReader();
			reader.setOnReadingFileListener(mOnReadingFileListener);
			// setup progress dialog
			sProgressDialog.setProgress(0);
			sProgressDialog.setMax((int) reader.getFileSize());
			sProgressDialog.setMessage((new File(mLastFilename)).getName());
			sProgressDialog.show();

			// new thread to handle parsing
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						// add parsed data to current app configuration
						Config.lastData = parser.parse();
						Config.setQuestionLangId(null);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					// initialize UI once parsing has finished
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							sProgressDialog.dismiss();
							initialize();
						}
					});
				}
			}).start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.file_menu, menu);
		mClearStatsMenu = menu.getItem(0);
		return true;
	}

	/**
	 * Initializes main KVTML menu.
	 */
	private void initialize()
	{
		if (Config.lastData == null || Config.lastData.version == null || !Config.lastData.version.equals(Kvtml.VERSION))
		{
			Toast.makeText(this, R.string.error_unsupported_kvtml_format, Toast.LENGTH_LONG)
				.show();
			Config.removeRecentFile(Config.inputFile.getAbsolutePath());
			finish();
			return;
		}

		if (Config.lastData.identifiers == null || Config.lastData.identifiers.size() == 0)
		{
			Toast.makeText(this, R.string.error_no_identifier, Toast.LENGTH_LONG)
				.show();
			Config.removeRecentFile(Config.inputFile.getAbsolutePath());
			finish();
			return;
		}

		boolean oneLanguage = Config.lastData.identifiers.size() == 1;
		if (oneLanguage)
			// we cannot randomize between languages if there's only one
			Config.randomize = false;

		setTitle(R.string.title_back_to_main_menu);

		TextView tv = (TextView) findViewById(R.id.text_filename);
		tv.setText(Config.inputFile.getName().replace(".kvtml", ""));

		// multiple choice menu
		Button btn = (Button) findViewById(R.id.button_multiple_choice);
		if (!oneLanguage)
			btn.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (Config.lastData.lessons.isEmpty())
						startActivity(new Intent(VocabFileActivity.this, MultipleChoiceActivity.class));
					else
					{
						Intent i = new Intent(VocabFileActivity.this, FilterActivity.class);
						i.putExtra(FilterActivity.TARGET_ACTIVITY,
							MultipleChoiceActivity.class.getName());
						startActivity(i);
					}
				}
			});
		else
			btn.setVisibility(View.GONE);

		// question and answer menu
		btn = (Button) findViewById(R.id.button_qna);
		if (!oneLanguage)
			btn.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (Config.lastData.lessons.isEmpty())
						startActivity(new Intent(VocabFileActivity.this, QuestionAnswerActivity.class));
					else
					{
						Intent i = new Intent(VocabFileActivity.this, FilterActivity.class);
						i.putExtra(FilterActivity.TARGET_ACTIVITY,
							QuestionAnswerActivity.class.getName());
						startActivity(i);
					}
				}
			});
		else
			btn.setVisibility(View.GONE);

		// flashcard menu
		btn = (Button) findViewById(R.id.button_fc);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (Config.lastData.lessons.isEmpty())
						startActivity(new Intent(VocabFileActivity.this, FlashcardActivity.class));
				else
				{
					Intent i = new Intent(VocabFileActivity.this, FilterActivity.class);
					i.putExtra(FilterActivity.TARGET_ACTIVITY,
						FlashcardActivity.class.getName());
					startActivity(i);
				}
			}
		});

		btn = (Button) findViewById(R.id.swap);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Config.swapLanguages();
			}
		});


		// browse menu
		btn = (Button) findViewById(R.id.button_browse);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				EntryBrowserDialog.data = Config.lastData;

				Intent i = new Intent(VocabFileActivity.this, EntryBrowserDialog.class);
				i.putExtra("windowTitle", Config.inputFile.getName().replace(".kvtml", ""));
				startActivity(i);
			}
		});

		// stats menu
		mStatsButton = (Button) findViewById(R.id.button_statistics);
		mStatsButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final StatsOpenHelper stats = StatsOpenHelper.getInstance(VocabFileActivity.this, -1);
				long fileId = stats.getFileId(sFilename);
				if (fileId < 1)
					return;
				HashMap<String, Kvtml.Entry> entries = stats.getTopMistaken(fileId, 5);
				if (entries == null)
					return;
				Resources res = VocabFileActivity.this.getResources();

				EntryBrowserDialog.data = Config.lastData.cloneFromEntries(entries);
				Intent i = new Intent(VocabFileActivity.this, EntryBrowserDialog.class);
				i.putExtra("windowTitle", res.getString(R.string.title_stats));
				startActivity(i);
			}
		});

		toggleMenuButtons();
		mMenuLayout.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		/*
		 * Ask the user if she really wants to quit the test.
		 *
		 * This is handy since some ppl may think that pressing the back button
		 * will take them back to the previous question.
		 */
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
			return goHome();
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				return goHome();
			case R.id.option_reset_file_stats:
				new AlertDialog.Builder(this)
					.setTitle(R.string.menu_reset_file_stats)
					.setMessage(R.string.message_really_clear_file_stats)
					.setPositiveButton(R.string.button_yes,
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								StatsOpenHelper stats = StatsOpenHelper.getInstance(
									VocabFileActivity.this, 0);
								long fileId = stats.getFileId((new File(mLastFilename))
									.getName());
								stats.clearFileStats(fileId);
							}
						}).setNegativeButton(R.string.button_no, null).show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public boolean goHome()
	{
		Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
		return true;
	}

	@Override
	protected void toggleMenuButtons()
	{
		// if onCreate() wasn't called yet
		if (mStatsButton != null)
		{
			StatsOpenHelper stats = StatsOpenHelper.getInstance(getApplicationContext(), -1);
			boolean b = stats.hasStats(mLastFilename);
			mStatsButton.setVisibility(b ? View.VISIBLE : View.GONE);
			if (mClearStatsMenu != null)
				mClearStatsMenu.setVisible(b);
		}
		super.toggleMenuButtons();
	}

	public void onClickSyns(View button) {
		// Betrag:

		Log.d("MyApp","I am here");
	}

}