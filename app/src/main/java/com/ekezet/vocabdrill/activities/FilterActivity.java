package com.ekezet.vocabdrill.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.activities.test_types.FlashcardActivity;
import com.ekezet.vocabdrill.activities.test_types.MultipleChoiceActivity;
import com.ekezet.vocabdrill.activities.test_types.QuestionAnswerActivity;
import com.ekezet.vocabdrill.adapters.LessonExpListAdapter;
import com.ekezet.vocabdrill.dialogs.EntryBrowserDialog;
import com.ekezet.vocabdrill.helpers.EntriesFilter;
import com.ekezet.vocabdrill.helpers.Gradient;
import com.ekezet.vocabdrill.kvtml.Kvtml;

public class FilterActivity extends Activity
{
	public final static String TARGET_ACTIVITY = "target_activity";
	public final static String EXTRA_FILTER = "extra_filter";

	private Spinner spinnerStart, spinnerEnd;
	private ArrayAdapter<CharSequence> adapterStart, adapterEnd;
	private RadioButton mRadioLeitner, mRadioLearn;
	private ExpandableListView expListView;
	LessonExpListAdapter mExpListAdptr;

	HashMap<String, Kvtml.Lesson> selectedLessons;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN,
			LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_filter);

		// getting target activity from the intent
		Intent i = getIntent();
		final String targetActivity = (String) i
			.getSerializableExtra(TARGET_ACTIVITY);

		// expandable list view
		expListView = (ExpandableListView) findViewById(R.id.explistview_lesson_filter);
		// set the header of the expandable list view
		LayoutInflater inflater = getLayoutInflater();
		ViewGroup expListHeader = (ViewGroup) inflater.inflate(R.layout.header_exp_list,
			expListView, false);
		if (targetActivity.equals(EntryBrowserDialog.class.getName()))
		{
			LinearLayout practicePrefs = (LinearLayout) expListHeader
				.findViewById(R.id.layout_practice_prefs);
			practicePrefs.setVisibility(View.GONE);
		}
		mExpListAdptr = new LessonExpListAdapter(this, Config.lastData.lessons);
		expListView.addHeaderView(expListHeader);
		expListView.setAdapter(mExpListAdptr);

		mRadioLeitner = (RadioButton) findViewById(R.id.radio_leitner);
		mRadioLearn = (RadioButton) findViewById(R.id.radio_learn);
		if (!targetActivity.equals(FlashcardActivity.class.getName()))
			mRadioLearn.setVisibility(View.GONE);

		final Button doneButton = (Button) findViewById(R.id.button_done);
		doneButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (!entriesInFilter())
				{
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.selected_dataset_empty),
						Toast.LENGTH_LONG).show();
					return;
				}

				final EntriesFilter filter = new EntriesFilter();
				// get level range
				int start = Integer.valueOf((String) spinnerStart.getSelectedItem());
				int end = Integer.valueOf((String) spinnerEnd.getSelectedItem());
				if (start > end)
				{
					int tmp = end;
					end = start;
					start = tmp;
				}

				filter.add(EntriesFilter.FILTER_GRADE_RANGE,
					String.format("%d:%d", start, end));

				if (mRadioLeitner.isChecked())
					filter.add(EntriesFilter.FILTER_LEITNER, String.valueOf(true));

				if (targetActivity.equals(QuestionAnswerActivity.class.getName()))
				{
					Intent intent = new Intent(FilterActivity.this,
						QuestionAnswerActivity.class);
					intent.putExtra(EXTRA_FILTER, filter);
					startActivity(intent);
				} else if (targetActivity.equals(MultipleChoiceActivity.class.getName()))
				{
					Intent intent = new Intent(FilterActivity.this,
						MultipleChoiceActivity.class);
					intent.putExtra(EXTRA_FILTER, filter);
					startActivity(intent);
				} else if (targetActivity.equals(FlashcardActivity.class.getName()))
				{
					Intent intent = new Intent(FilterActivity.this,
						FlashcardActivity.class);
					intent.putExtra(EXTRA_FILTER, filter);
					intent.putExtra(FlashcardActivity.LEARN_MODE, mRadioLearn.isChecked());
					startActivity(intent);
				}
			}
		});
		List<View> views = new ArrayList<View>();
		views.add(doneButton);
		Gradient.colorize(views, 1);

		// spinners
		spinnerStart = (Spinner) findViewById(R.id.spinner_start);
		adapterStart = ArrayAdapter.createFromResource(this,
			R.array.leitner_levels, android.R.layout.simple_spinner_item);
		adapterStart
			.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerStart.setAdapter(adapterStart);
		spinnerStart.setSelection(0);

		spinnerEnd = (Spinner) findViewById(R.id.spinner_end);
		adapterEnd = ArrayAdapter.createFromResource(this, R.array.leitner_levels,
			android.R.layout.simple_spinner_item);
		adapterEnd
			.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerEnd.setAdapter(adapterEnd);
		spinnerEnd.setSelection(7);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.filter_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		super.onPrepareOptionsMenu(menu);
		if (Config.lastData.lessons.isEmpty())
		{
			MenuItem item = (MenuItem) menu.findItem(R.id.option_toggle_lessons);
			item.setVisible(false);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.option_toggle_lessons:
				HashMap<String, Kvtml.Lesson> lessons = Config.lastData.lessons;
				Boolean state = !lessons.get("0").inpractice;
				for (Kvtml.Lesson lesson : lessons.values())
				{
					lesson.inpractice = state;
					for (Kvtml.Lesson sublesson : lesson.subLessons.values())
					{
						sublesson.inpractice = state;
					}
				}
				mExpListAdptr.notifyDataSetChanged();
				return true;
		}
		return false;
	}

	private Boolean entriesInFilter()
	{
		for (Kvtml.Lesson lesson : Config.lastData.lessons.values())
		{
			for (Kvtml.Lesson sublesson : lesson.subLessons.values())
			{
				if (sublesson.keys.size() != 0 && sublesson.inpractice)
					return true;
			}
			if (lesson.keys.size() != 0 && lesson.inpractice)
				return true;
		}
		return false;
	}
}
