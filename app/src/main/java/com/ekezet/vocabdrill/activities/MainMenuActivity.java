package com.ekezet.vocabdrill.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.dialogs.DbxOpenDialog;
import com.ekezet.vocabdrill.dialogs.FileOpenDialog;
import com.ekezet.vocabdrill.dialogs.NewsDialog;
import com.ekezet.vocabdrill.dialogs.RecentFilesDialog;
import com.ekezet.vocabdrill.helpers.DbxTools;

import java.io.File;

public class MainMenuActivity extends BaseMenuActivity
{
	private final static int OPEN_FILE = 1000;
	private final static int OPEN_DBX_FILE = 1001;
	private final static int OPEN_RECENT = 2000;

	private Button mOpenRecentFileButton;
	private Button mDbxOpenFileButton;

	private static boolean sTouchable = false;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		final Bundle savedState = savedInstanceState;

		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					Config.load(getApplicationContext());
					// splash screen delay
					Thread.sleep(333);
					// call main app initialization
					initialize(savedState);
				}
				catch (InterruptedException e)
				{
				}
			}
		});
	}

	private void initialize(Bundle savedInstanceState)
	{
		/*
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
		*/
		setTheme(R.style.AppTheme);
		setContentView(R.layout.activity_main);

		setTitle(String.format("v%s", Config.getVersionName()));

		// open new file button
		Button btn = (Button) findViewById(R.id.button_open_file);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				startActivityForResult(new Intent(MainMenuActivity.this,
					FileOpenDialog.class), OPEN_FILE);
			}
		});

		// open from dropbox button
		mDbxOpenFileButton = (Button) findViewById(R.id.button_dbx_open_file);
		mDbxOpenFileButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				startActivityForResult(new Intent(MainMenuActivity.this,
					DbxOpenDialog.class), OPEN_DBX_FILE);
			}
		});

		// get handle to 'open last file' button
		mOpenRecentFileButton = (Button) findViewById(R.id.button_open_recent_file);
		mOpenRecentFileButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				startActivityForResult(new Intent(MainMenuActivity.this,
					RecentFilesDialog.class), OPEN_RECENT);
			}
		});

		// settings button
		btn = (Button) findViewById(R.id.button_settings);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				startActivity(new Intent(MainMenuActivity.this, PreferencesActivity.class));
			}
		});

		// quit button
		btn = (Button) findViewById(R.id.button_quit);
		btn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				kill();
			}
		});

		toggleMenuButtons();
		mMenuLayout.setVisibility(View.VISIBLE);
		sTouchable = true;

		if (Config.getPreviousVersionCode() < Config.getVersionCode()
				&& !Config.getPreviousVersionName().equals(Config.getVersionName()))
		{
			Intent i = new Intent(this, NewsDialog.class);
			i.putExtra("limit", 3);
			Config.invalidatePreviouseVersionCode();
			Config.invalidatePreviouseVersionName();
			Config.save();
			startActivity(i);
		}
	}


	private void kill()
	{
		Config.save();
		try
		{
			finalize();
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		System.exit(0);
	}

	/**
	 * Handles the results returned by dialogs.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (data == null)
			return;
		if (resultCode != Activity.RESULT_OK)
			return;

		String selected = "";
		switch (requestCode)
		{
			case OPEN_RECENT:
				selected = data.getDataString();
			case OPEN_FILE:
				selected = String.format("%s", selected).equals("") ?
						data.getStringExtra(FileOpenDialog.INTENT_PATH_PARAMETER) : selected;
			case OPEN_DBX_FILE:
				selected = String.format("%s", selected).equals("") ?
						data.getStringExtra(DbxOpenDialog.INTENT_PATH_PARAMETER) : selected;
				Config.inputFile = new File(selected);
				Config.insertRecentFile(Config.inputFile.getAbsolutePath());
				Config.saveRecentFilesList();
				startActivity(new Intent(this, VocabFileActivity.class));
		}
	}

	@Override
	protected void toggleMenuButtons()
	{
		// show/hide recent files menu
		mOpenRecentFileButton.setVisibility(Config.hasRecentFiles() ? View.VISIBLE : View.GONE);
		// show/hide dropbox browser
		final ConnectivityManager connMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ninfo = connMan.getActiveNetworkInfo();
		boolean showDbx = ninfo != null && ninfo.isConnectedOrConnecting() && DbxTools.isLinked();
		mDbxOpenFileButton.setVisibility(showDbx ? View.VISIBLE : View.GONE);
		updateMenuColours();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (!sTouchable)
			return false;
		return super.onTouchEvent(event);
	}
}
