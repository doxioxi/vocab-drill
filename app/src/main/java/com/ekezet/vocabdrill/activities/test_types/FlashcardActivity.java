package com.ekezet.vocabdrill.activities.test_types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.Question;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.dialogs.EntryBrowserDialog;
import com.ekezet.vocabdrill.helpers.Gradient;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.kvtml.KvtmlWriter;

public class FlashcardActivity extends TestActivity
{
	/**
	 * this mode shows both question and answer at the same time; used only for
	 * consistancy
	 */
	final public static String LEARN_MODE = "learnmode";
	/**
	 * this mode only shows the question
	 */
	final public static String VIEW_MODE = "viewmode";
	/**
	 * this mode shows the answer
	 */
	final public static String ANSWER_MODE = "answermode";

	protected int mTestType = TEST_FLASHCARD;

	/**
	 * shows the answer
	 */
	private TextView mAnswerText;
	/**
	 * shows a comment
	 */
	private TextView mCommentText;
	/**
	 * left button: "answer question", "knew the answer" and "previous question"
	 */
	private Button mButtonLeft;
	/**
	 * right button: "answer this question later", "didn't know the answer" and
	 * "next question"
	 */
	private Button mButtonRight;

	/**
	 * learn mode shows both question and answers and lets the user cycle forwards
	 * and backwards
	 */
	private Boolean mLearningMode = false;

	/**
	 * for learnmode: position in hashmap.keySet().toArray() to be able to
	 * navigate
	 */
	private int mPosition = 0;
	private ArrayList<String> mEntryIds = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Intent i = getIntent();
		if (i.hasExtra(LEARN_MODE))
			mLearningMode = (Boolean) i.getSerializableExtra(LEARN_MODE);

		mAnswerText = (TextView) findViewById(R.id.text_fc_answer);
		mCommentText = (TextView) findViewById(R.id.text_fc_comment);
		mButtonLeft = (Button) findViewById(R.id.button_fc_left);
		mButtonRight = (Button) findViewById(R.id.button_fc_right);

		if (mLearningMode)
		{
			mButtonLeft.setText(R.string.button_fc_back);
			mButtonLeft.setTag(LEARN_MODE);
			mButtonLeft.setEnabled(false);
			mButtonRight.setText(R.string.button_fc_next);
			mButtonRight.setTag(LEARN_MODE);
			mAnswerText.setVisibility(View.VISIBLE);
			mCommentText.setVisibility(View.VISIBLE);
		}

		// initialize data, has to be done before setting the onClickListener
		restart();

		// left button: "answer question", "knew the answer" and "previous question"
		mButtonLeft.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (((String) mButtonLeft.getTag()).equals(VIEW_MODE))
				{
					// user tries to answer the question
					mButtonLeft.setTag(ANSWER_MODE);
					mButtonLeft.setText(R.string.button_fc_right);
					mButtonRight.setTag(ANSWER_MODE);
					mButtonRight.setText(R.string.button_fc_wrong);
					mAnswerText.setVisibility(View.VISIBLE);
					mCommentText.setVisibility(View.VISIBLE);
					recolorize();
				} else if (((String) mButtonLeft.getTag()).equals(ANSWER_MODE))
				{
					// user knew the answer
					mButtonLeft.setTag(VIEW_MODE);
					mButtonLeft.setText(R.string.button_fc_answer);
					mButtonRight.setTag(VIEW_MODE);
					mButtonRight.setText(R.string.button_fc_next);
					mAnswerText.setVisibility(View.GONE);
					mCommentText.setVisibility(View.GONE);
					recolorize();
					if (correct(mQuestion.getSolution()))
						updateQuestion();
				} else if (mLearningMode)
				{
					// user wants to see the previous question
					if (mPosition == 1)
						mButtonLeft.setEnabled(false);
					else if (mPosition == mMaxQuestions - 1)
						mButtonRight.setEnabled(true);
					mPosition--;
					updateQuestion();
				}
			}
		});
		// right button: "answer this question later", "didn't know the answer" and
		// "next question"
		mButtonRight.setOnClickListener(
			new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (((String) mButtonRight.getTag()).equals(VIEW_MODE))
					{
						// user wants to answer this question later
						mQuestion = mQuestionFactory.create(mQuestionData, mMaxItems);
						updateQuestion();
					} else if (((String) mButtonRight.getTag()).equals(ANSWER_MODE))
					{
						// user didn't know the answer
						mButtonLeft.setTag(VIEW_MODE);
						mButtonLeft.setText(R.string.button_fc_answer);
						mButtonRight.setTag(VIEW_MODE);
						mButtonRight.setText(R.string.button_fc_next);
						mAnswerText.setVisibility(View.GONE);
						mCommentText.setVisibility(View.GONE);
						recolorize();
						mistake(mQuestion.getSolution());
						updateQuestion();
					} else if (mLearningMode)
					{
						// user wants to see the next question
						if (mPosition == 0)
							mButtonLeft.setEnabled(true);
						else if (mPosition == mMaxQuestions - 2)
							mButtonRight.setEnabled(false);
						mPosition++;
						updateQuestion();
					}
				}
			});

		recolorize();
	}

	@Override
	protected Question getNewQuestion(HashMap<String, Kvtml.Entry> entries, int maxItems)
	{
		if (mLearningMode)
		{
			if (mEntryIds == null)
			{
				// create a sorted array of entry ids for navigation
				Set<String> entryKeySet = entries.keySet();
				mEntryIds = new ArrayList<String>();
				mEntryIds.addAll(entryKeySet);
				Collections.sort(mEntryIds);
			}
			// use an array so we can navigate forwards and backwards
			Kvtml.Entry solution = entries.get(mEntryIds.get(mPosition));
			return new Question(entries, solution, null);
		} else
			return super.getNewQuestion(entries, maxItems);
	}

	@Override
	protected void loadContentView()
	{
		setContentView(R.layout.activity_flashcard);
	}

	@Override
	protected void updateQuestion(boolean configChanged)
	{
		super.updateQuestion(configChanged);
		mAnswerText.setText("");
		mCommentText.setText("");
		Kvtml.Entry solution = mQuestion.getSolution();
		if (solution == null)
		{
			mAnswerText.setText("???");
			return;
		}
		Kvtml.Translation answer = solution.translations.get(Config.getAnswerLangId());
		if (answer == null)
		{
			mAnswerText.setText("???");
			return;
		}
		// additional views
		mAnswerText.setText(String.valueOf(answer.text));
		mCommentText.setText(String.valueOf(answer.comment));
	}

	@Override
	protected String getTitleString()
	{
		Kvtml.Entry solution = mQuestion.getSolution();
		if (mLearningMode)
			return String.format("%d/%d - Level: %d", 1 + mPosition, mMaxQuestions, solution.currentGrade);
		else
			return String.format("%d/%d - Level: %d",
				1 + (mMaxQuestions - mEntriesLeft), mMaxQuestions, solution.currentGrade);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.option_save)
		{
			// adds an option to save during practice
			KvtmlWriter saver = new KvtmlWriter(Config.lastData, "Vocabdrill.kvtml");
			saver.save();
			return true;
		} else
			return super.onOptionsItemSelected(item);
	}

	@Override
	protected int getMenuResource()
	{
		// use a different menu to replace "showAnswer" with "save"
		return R.menu.flashcard_menu;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (!Config.confirmProgress)
			return super.onKeyDown(keyCode, event);

		/*
		 * Ask the user if she really wants to quit the test.
		 *
		 * This is handy since some ppl may think that pressing the back button
		 * will take them back to the previous question.
		 */
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
		{
			new AlertDialog.Builder(this)
				.setTitle(R.string.title_really_quit)
				.setMessage(R.string.message_really_quit)
				.setPositiveButton(R.string.button_yes,
					new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							new AlertDialog.Builder(getApplicationContext())
								.setTitle(R.string.title_really_quit)
								.setMessage(R.string.message_save)
								.setPositiveButton(R.string.button_yes,
									new DialogInterface.OnClickListener()
									{
										@Override
										public void onClick(DialogInterface dialog,
											int which)
										{
											KvtmlWriter saver = new KvtmlWriter(Config.lastData,
												"Vocabdrill.kvtml");
											saver.save();
											finish();
										}
									})
								.setNegativeButton(R.string.button_no,
									new DialogInterface.OnClickListener()
									{
										@Override
										public void onClick(DialogInterface dialog, int which)
										{
											finish();
										}
									}).show();
						}
					}).setNegativeButton(R.string.button_no, null).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void showFinishDialog()
	{
		if (mMistakesNum != 0)
		{
			EntryBrowserDialog.data = Config.lastData.cloneFromEntries(mMistakes);
			Intent i = new Intent(FlashcardActivity.this, EntryBrowserDialog.class);
			i.putExtra("windowTitle", getResources().getString(R.string.title_show_mistakes));
			startActivity(i);
		}

		if (!Config.confirmRestart)
			return;

		if (Config.confirmProgress)
			new AlertDialog.Builder(FlashcardActivity.this)
				.setMessage(R.string.message_save_progress)
				.setTitle(R.string.title_test_ended)
				.setCancelable(false)
				.setPositiveButton(R.string.button_yes,
					new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int id)
						{
							KvtmlWriter saver = new KvtmlWriter(Config.lastData, Config.PROGRESS_FILE);
							saver.save();
							finish();
						}
					})
				.setNegativeButton(R.string.button_no,
					new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int id)
						{
							finish();
						}
					}).create().show();
		else
		{
			KvtmlWriter saver = new KvtmlWriter(Config.lastData, Config.PROGRESS_FILE);
			saver.save();
			finish();
		}
	}

	private void recolorize()
	{
		final ArrayList<View> vs = new ArrayList<View>();
		vs.add(mLangText);
		vs.add(mQuestionText);
		if (mAnswerText.getVisibility() == View.VISIBLE)
			vs.add(mAnswerText);
		if (mCommentText.getVisibility() == View.VISIBLE)
			vs.add(mCommentText);
		vs.add(mButtonLeft);
		vs.add(mButtonRight);
		Gradient.colorize(vs);
	}
}